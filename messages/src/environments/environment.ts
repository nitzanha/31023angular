// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDTT1BM943ysT4dq3afhCywRkK468lY6vY",
    authDomain: "users-ea8c9.firebaseapp.com",
    databaseURL: "https://users-ea8c9.firebaseio.com",
    projectId: "users-ea8c9",
    storageBucket: "users-ea8c9.appspot.com",
    messagingSenderId: "380872794156"
  }
};
