import { Injectable } from '@angular/core';
import {Http, Headers} from '@angular/http';
import {HttpParams} from '@angular/common/http';
import {environment } from './../../environments/environment';
import {AngularFireDatabase} from 'angularfire2/database';


@Injectable()
export class UsersService {
  http:Http;
  
  getUsers(){
  //get users from the slim rest api (Don't say DB)
  return this.http.get('http://localhost/31023/slim/users');
}

  deleteUser(key){ 
    return this.http.delete('http://localhost/31023/Slim/users/' +key);
  }

  getUser(id){
    return this.http.get('http://localhost/31023/Slim/users/' +id);
  }
  
  postUser(data){
    let options = {
      headers:new Headers({'content-type':'application/x-www-form-urlencoded'})
    }
    let params = new HttpParams().append('name',data.name).append('phoneNumber',data.phoneNumber);
    return this.http.post('http://localhost/31023/Slim/users',params.toString(),options);
  }

  putUser(data,id){
    let options = {
      headers: new Headers({
        'content-type':'application/x-www-form-urlencoded'
      })
    }
    let params = new HttpParams().append('name',data.name).append('phoneNumber',data.phoneNumber);
    return this.http.put('http://localhost/31023/Slim/users/'+ id,params.toString(), options);
  }

  getUsersFire(){
        return this.db.list('/users').valueChanges();
      }


  constructor(http:Http, private db:AngularFireDatabase) {
    this.http = http;
   }

}

