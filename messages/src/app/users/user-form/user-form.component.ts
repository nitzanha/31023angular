import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UsersService } from '../users.service';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {
  @Output() addUser: EventEmitter<any> = new EventEmitter<any>();
  @Output() addUserPs: EventEmitter<any> = new EventEmitter<any>();

  service:UsersService;
  
    usrform = new FormGroup({
      name:new FormControl(),
      phoneNumber:new FormControl(),
      id:new FormControl()
    });

  constructor(private route: ActivatedRoute ,service: UsersService) {
    this.service = service;
   }
   
  //שליחת הנתונים
  sendData() {
    //הפליטה של העדכון לאב
  this.addUser.emit(this.usrform.value.message);
  console.log(this.usrform.value);
  this.route.paramMap.subscribe(params=>{
    let id = params.get('id');
    this.service.putUser(this.usrform.value, id).subscribe(
      response => {
        console.log(response.json());
        this.addUserPs.emit();
      }
    );
  })
  }

 user;
  ngOnInit() {
    this.route.paramMap.subscribe(params=>{
      let id = params.get('id');
      console.log(id);
      this.service.getUser(id).subscribe(response=>{
        this.user = response.json();
        console.log(this.user);
      })
  })

  }

}
